defmodule AlbertoLib do
  defmodule Map do
    @moduledoc """
    Documentation for `Alberto.Map`.
    """

    @doc """
    Transform atom key to string key

    """
    def atom_key_to_string(a) do
      for {key, val} <- a, into: %{}, do: {Atom.to_string(key), val}
    end
  end

  defmodule Struct do
    def string_key_to_atom(a) do
      for {key, val} <- a, into: %{}, do: {String.to_atom(key), val}
    end
  end

  defmodule Node do
    def alberto_node_m(node), do: {:node_m, node}
    def alberto_path_m(path), do: {:bypath_m, path}
    def alberto_get_path_m(node_path, path), do: {:get_m, node_path, path}
    def alberto_content_m(uniqueid), do: {:nodecontent_m, uniqueid}
  end

  defmodule User do
    require Monad.Error, as: Error
    import Error

    def alberto_node_m(username), do: {:username, username}
    def alberto_login_password(username, passsword), do: {:login, username, passsword}
    def alberto_sessiones(ticket), do: {:ticket, ticket}

    def alberto_user_email(%{"data" => %{"email" => email}}), do: Error.return(email)
    def alberto_user_email(a), do: Error.fail({:user_no_email, a})
  end

  defmodule Net do
    require Monad.Error, as: Error
    import Error

    def get_closest_pid_m(a) do
      case :pg.get_members(a) do
        [] -> Error.fail(:no_such_group)
        e -> Enum.random(e) |> Error.return()
      end
    end
  end

  defmodule Monadm do
    def value({:ok, v}), do: v
    def value({:error, v}), do: v
  end

  defmodule S do
    require Monad.Error, as: Error

    def call(a, server) do
      Error.m do
        pid <- server |> AlbertoLib.Net.get_closest_pid_m()
        pid |> GenServer.call(a)
      end
    end
  end
end
